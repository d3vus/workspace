
print("Hello World") 

def hello(x) -> str :
    print("Hi, how are you", x, "?")
    return x

hello(3)

o = hello(3)

print(o)
