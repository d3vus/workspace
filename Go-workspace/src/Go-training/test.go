package main

import (
	"fmt"
	"bufio"
	"os"
)

func main() {

	input := bufio.NewReader(os.Stdin)

	name, _ := input.ReadString('\n')
	
	fmt.Print(name)

}
