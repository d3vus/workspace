package main

import "fmt"

func main() {

	fmt.Println("heoo")

	s1 := "heooooo"

	// byte is uint8

	bs1 := []byte(s1)

	fmt.Println(bs1)

	fmt.Printf("Ascii value of %v is %v and bs1 is of type %T\n",s1,bs1,bs1)


	for i:=0 ; i<len(s1) ; i++ {
		fmt.Printf("%#X\n",s1[i])
	}

	for i,v := range s1{

		fmt.Println(i,v)

		fmt.Printf("for %q ascii value is %d\n and hexadecimal value is %#X",v,v,v)

	}


	
}
