// Variable in go - how to define them and use them

package main

import "fmt"

// Package level variable define

// General level declaration
var x int = 42

// Declare value but without type

var y = "hello"

// Multiple variable declaration of same type

var v, w float64 = 89, 78

// Define multiple variable using different types

var (
	a int     = 4
	b string  = "op"
	c float32 = 67.89
	d bool    = true
)

func main() {

	fmt.Println(v, w)
	fmt.Printf("%T\t%T\n", w, v)

	fmt.Println(a, b, c, d)
	fmt.Printf("%T\t%T\t%T\t%T\n", a, b, c, d)

	// short variable define
	// cannot be used in package level declaration

	i := 3
	j := "loop"
	k := 4.56

	fmt.Println(i,j,k)

	// use operations as value

	l := 3 + 4

	fmt.Println(l)

	// Declare multiple variable at same time
	p, q := 6, 8

	fmt.Println(p,q)

	// swap values like python but both variable have to be same type

	p , q = q , p

	fmt.Println(p,q)
}
