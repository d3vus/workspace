package main

import "fmt"

// Lv. 2

var (a int = 45; b string = "Hello my son " ; c bool = false)

// Lv. 4
type man int

// Lv. 5
var p int


func main() {

	// Lv. 1.

	x := 42
	y := "James Bond"
	z := true

	fmt.Println(x,y,z)


	fmt.Println(x)
	fmt.Println(y)
	fmt.Println(z)



	// Lv. 2.

	fmt.Println(a,b,c)

	// Lv. 3

	s := fmt.Sprintf("%v\t%v\t%v\n", a,b,c)

	fmt.Println(s)

	// Lv. 4

	var w man

	fmt.Printf("value of w --> %v\tType of w --> %T\n",w,w)

	w = 42

	fmt.Printf("value of w --> %v\tType of w --> %T\n",w,w)


	// Lv. 5

	fmt.Printf("value of p --> %v\tType of p --> %T\n",p,p)
	
	r := int(w)

	p = r

	fmt.Printf("value of p --> %v\tType of p --> %T\n",p,p)

	
}
