// fmt package -->> provides std i/o for the program 

package main

import "fmt"

func main() {
	fmt.Println("Hello World")

	var (
		a = 61;
		b = "string";
		c = 3.6;
		d = true
	)
	// get the type of the vakue declared

	fmt.Printf("%T\t%T\t%T\t%T\n",a,b,c,d)

	// get the value of variables Printf

	fmt.Printf("%v\n",d)

	// get the value of different variable in int(binary format) format

	fmt.Printf("%b\n",c)

	// get the value of variable in Hexadecimal format

	fmt.Printf("%X\n",a)

	// get the value of variable in Unicode format

	fmt.Printf("%#x\n",a)

	// O/p in large X format
	fmt.Printf("%#X\n",a)


	// Now using Sprintf

	s := fmt.Sprintf("%x",a)

	fmt.Printf("%v ==>> %T\n",s,s)


	// Inputs in go

	var p,q int

	x,y := fmt.Scanln(&p,&q)

	fmt.Println(x,y)
	
}
