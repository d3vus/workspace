package main

import "fmt"

const a int = 23

const (
	x = "jio"
	y = false
	z = 45
)

func main() {
	fmt.Println(a)

	fmt.Printf("%T\n%T\n%T\n",x,y,z)
}
