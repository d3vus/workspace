package main

import "fmt"


var x bool

func main() {
	fmt.Println(x)

	x = true

	fmt.Println(x)

	// Comparison in go

	a := 32
	b := 78 
	
	fmt.Println(a>b, a==b , a<b , a>=b , a<=b)
}
