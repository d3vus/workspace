package main

import "fmt"

const (
	a = true
	b = iota
	c = 4.9

)

const x = 67

const y = 45


// iota

const (

	i0 = iota
	i1 = iota
	i2 = iota
)

// more iota

const (

	i4 = iota
	i5
	i6
)

const i7 = iota
const i8 = 0
const i9 = 0

const (

	i10 = iota
	i11
	i12
	i13 = iota
	i14
	i15
)

const (
	i16 = iota + 1
	i17 = iota + 1
	i18 = iota + 1
)


// Can not use iota with var , it only works with constant

func main() {

	fmt.Println(i0,i1,i2)

	fmt.Println(a,b,c)
	fmt.Printf("%T\t%T\t%T\n",a,b,c)

	fmt.Println(i4,i5,i6)

	fmt.Println(i7,i8,i9)

	fmt.Println(i10,i11,i12,i13,i14,i15)

	fmt.Println(i16,i17,i18)

}
