package main

import "fmt"


// Go-lang has many types int,string,float,bool etc.
// but you can define your own too (type ofcourse)

var a int = 42

// create an type goofy which have an underline type int
type goofy int

// create a variable called of type goofy
var b goofy

func main() {

	b = 2

	fmt.Println(b)

	fmt.Printf("%T\n",b)

	// we can convert one type to other with some limitation

	a = int(b)

	var c int = 34

	fmt.Printf("%T\n",c)
	
	d := string(c)

	fmt.Printf("%T\n",d)
	
}
